const getSum = (str1, str2) => {
  let resStr = "";

  if(str1 === '' || str2 === ''){
    return str1 + str2;
  }

  if(isNaN(str1) || isNaN(str2)){
    return false;
  }

  for (let i = 0; i < str1.length && i < str2.length; i++) {
    {
      resStr += Number(str1[i]) + Number(str2[i]);
    }
  }

  return resStr == '' ? false : resStr;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postsCounter = 0, commentsCounter = 0;

  for (const elem of listOfPosts) {
    if(elem.author === authorName){
      if(elem.post)
        postsCounter++;
    }

    if(elem.comments){
      for (const c of elem.comments) {
        if(c.author === authorName)
          commentsCounter++;
      }
    }
  }

  return "Post:" + postsCounter + ",comments:" + commentsCounter;
};

const tickets= (people)=> {
  if(people[0] !== 25){
    return "NO";
  }

  let counter25 = 0, counter50 = 0, counter100 = 0;

  for (const p of people) {
    if(p === 25){
      counter25++;
    }
    else if(p === 50){
      counter50++;
    }
    else{
      counter100++;
    }
  }

  if(counter25 <= counter50 || 
    (counter50 <= counter100 && counter50 > 2 * counter25)){
    return "NO";
  }

  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
